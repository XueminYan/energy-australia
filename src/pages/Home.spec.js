import { shallow, configure } from 'enzyme';
import React from 'react';
import Home from './Home';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('Home', () => {
	let component;

	// Cleanup mock
	afterEach(() => {
		if (component && component.unmount && component.exists()) component.unmount();
	});

	it('should render basic component', () => {
		component = shallow(<Home />);

		expect(component.exists()).toBe(true);
	});
});
