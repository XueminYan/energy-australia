import React, { useEffect, useState } from 'react';
import FestivalsService from '../services/festivals';
import { TreeItem, TreeView } from '@mui/lab';
import { ExpandMore as ExpandMoreIcon, ChevronRight as ChevronRightIcon } from '@mui/icons-material';

const Home = () => {
	const [error, setError] = useState('');
	const [loading, setLoading] = useState(false);
	const [records, setRecords] = useState('');

	const expandRecords = (arr) => {
		let expandedArray = [];
		arr.forEach((record) =>
			record.bands.forEach((band) => {
				expandedArray.push({
					recordLabel: band.recordLabel || 'Unknown Record Lable',
					bandName: band.name || 'Unknown Band Name',
					recordName: record.name || 'Unknown Record Name',
				});
			})
		);
		return expandedArray;
	};

	const groupBy = (arr, criteria) => {
		const newObj = arr.reduce((acc, currentValue) => {
			const property = currentValue[criteria];
			acc[property] = acc[property] || [];
			delete currentValue[criteria];
			acc[property].push(currentValue);
			return acc;
		}, {});
		return newObj;
	};

	useEffect(() => {
		const fetchFestivals = async () => {
			try {
				setLoading(true);
				const res = await FestivalsService.getFestivals();
				//Handle no data situation
				if (!res || res.length < 0) {
					throw new Error('there is no data');
				}
				//expand result
				let arr = expandRecords(res);
				//groupd array by record lable
				let groupedArray = groupBy(arr, 'recordLabel');

				//groupd array by band name
				for (const property in groupedArray) {
					groupedArray[property] = groupBy(groupedArray[property], 'bandName');
				}
				console.log(groupedArray);
				setRecords(groupedArray);
			} catch (error) {
				console.error(error);
				setError('Something went wrong, please try again later');
			} finally {
				setLoading(false);
			}
		};
		fetchFestivals();
	}, []);

	return (
		<div>
			{loading && <div>loading</div>}
			{error && <div>{error}</div>}
			{!loading && !error && records && (
				<TreeView aria-label="file system navigator" defaultCollapseIcon={<ExpandMoreIcon />} defaultExpandIcon={<ChevronRightIcon />} sx={{ flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}>
					{Object.keys(records)
						.sort()
						.map((label) => (
							<TreeItem label={label} nodeId={label} key={label}>
								{Object.keys(records[label])
									.sort()
									.map((band) => (
										<TreeItem label={band} nodeId={band} key={band}>
											{Object.keys(records[label][band])
												.sort()
												.map((festival) => (
													<TreeItem label={records[label][band][festival].recordName} nodeId={records[label][band][festival].recordName} key={records[label][band][festival].recordName} />
												))}
										</TreeItem>
									))}
							</TreeItem>
						))}
				</TreeView>
			)}
		</div>
	);
};

export default Home;
