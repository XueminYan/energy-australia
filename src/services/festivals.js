import { getInstance } from './tools';

class FestivalsService {
	static getFestivals = async () => {
		const res = await getInstance('/api/v1/festivals');
		return res.data;
	};
}

export default FestivalsService;
