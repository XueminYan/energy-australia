import axios from 'axios';
import { BASEURL } from './constants';

class NetworkError extends Error {
	constructor(code, message) {
		const fullMessage = message ? `${code}: ${message}` : code;
		super(fullMessage);
		this.name = code;
		this.code = code;
		this.message = fullMessage;
	}

	toString() {
		return this.message;
	}
}

export const apiInstance = axios.create({
	// Bypass CORS
	baseURL: BASEURL,
	timeout: 30000,
});

export const getInstance = async (url, params) => {
	try {
		const response = await apiInstance.get(url, { params: params });
		return response;
	} catch (error) {
		throw new NetworkError(error.response?.status, error.response?.data?.message ?? error.message);
	}
};
